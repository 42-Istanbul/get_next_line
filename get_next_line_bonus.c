#include "get_next_line_bonus.h"

static void	append(char **s1, char *s2)
{
	char	*tmp;

	tmp = ft_strjoin(*s1, s2);
	free(*s1);
	*s1 = tmp;
}

/*
**	@brief	Save the contents of s into other string
**
**	@param	s		string which contains newline or EOF
**	@return	char*	string with newline
*/
static char	*save_and_clear(char **line)
{
	char	*out;
	char	*tmp;
	char	*nl;

	nl = ft_strchr(*line, '\n');
	if (nl)
	{
		out = ft_substr(*line, 0, nl - *line + 1);
		tmp = ft_strdup(nl + 1);
		free(*line);
		*line = tmp;
		if (!**line)
		{
			free(*line);
			*line = NULL;
		}
		return (out);
	}
	out = ft_strdup(*line);
	free(*line);
	*line = NULL;
	return (out);
}

/*
**	@brief	Read data from fd and append it to line string until encountering
**	newline
**	@param	line	char pointer to persistent string
**	@param	fd		file descriptor of file
**	@return	int		last bytes read from fd
*/
static int	read_and_append(char **line, int fd)
{
	ssize_t	bytes_read;
	char	*buf;

	buf = (char *)malloc(sizeof(char) * (BUFFER_SIZE + 1));
	if (!buf)
		return (-1);
	bytes_read = read(fd, buf, BUFFER_SIZE);
	while (bytes_read > 0)
	{
		buf[bytes_read] = 0;
		if (!*line)
			*line = ft_strdup("");
		append(line, buf);
		if (ft_strchr(buf, '\n'))
			break ;
		bytes_read = read(fd, buf, BUFFER_SIZE);
	}
	free(buf);
	return (bytes_read);
}

/*
**	@brief	Get the next line object
**
**	@param	fd		file descriptor
**	@return	char*	line from fd with newline included
*/
char	*get_next_line(int fd)
{
	static char	*persistent[MAX_FD];
	char		*ret_line;
	int			br;

	if (fd < 0 || BUFFER_SIZE < 1 || fd > MAX_FD)
		return (NULL);
	br = read_and_append(&persistent[fd], fd);
	if (br < 0)
		return (NULL);
	if (!br && !persistent[fd])
		return (NULL);
	ret_line = save_and_clear(&persistent[fd]);
	return (ret_line);
}
