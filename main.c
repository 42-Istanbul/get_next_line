#include <stdio.h>
#include <fcntl.h>
#include "get_next_line.h"

int	main(void)
{
	int	fd = open("alice1.txt", O_RDONLY);
	char *line;
	int i;

	i = 1;
	while (i < 30)
	{
		(line = get_next_line(fd));
		printf("%2d| %s", i, line);
		if (!line)
			printf("\n");
		free(line);
		++i;
	}
	close(fd);
	return (0);
}
