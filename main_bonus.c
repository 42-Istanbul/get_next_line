#include <stdio.h>
#include <fcntl.h>
#include "get_next_line.h"

int	main(void)
{
	int	fd1 = open("alice1.txt", O_RDONLY);
	int	fd2 = open("alice2.txt", O_RDONLY);
	int	fd3 = open("alice3.txt", O_RDONLY);
	char *line;
	int i;

	i = 1;
	while (i < 22)
	{
		line = get_next_line(fd1);
		printf("fd_1 %2d| %s", i, line);
		if (!line)
			printf("\n");
		free(line);
		line = get_next_line(fd2);
		printf("fd_2 %2d| %s", i, line);
		if (!line)
			printf("\n");
		free(line);
		line = get_next_line(fd3);
		printf("fd_3 %2d| %s", i, line);
		if (!line)
			printf("\n");
		free(line);
		++i;
	}
	close(fd1);close(fd2);close(fd3);
	return (0);
}
